import React from 'react';
import {Header} from "./Components";
import {Home, Cart} from "./Pages";
import {Route} from "react-router-dom"
import * as axios from 'axios';
import {connect} from "react-redux";
import {setPizzas} from './Redux/actions/pizzas'


function App({items, setPizzas}) {


    React.useEffect(() => {
        axios.get('http://localhost:3000/db.json').then( res => setPizzas(res.data.pizzas) )
        // axios.get('https://e-shop-learning-react.netlify.app/db.json').then( res => setPizzas(res.data.pizzas) )
    }, [])

  return (
      <div className="wrapper">
            <Header />
          <div className="content">
              <Route exact path='/' render={() => <Home items={items} />}/>
              <Route exact path='/cart' render={() => <Cart />}/>
          </div>
      </div>
  );
}

const mapStateToProps = (state) => {
    return{
        items: state.pizzas.items
    }
}


export default connect(mapStateToProps, {setPizzas})(App);
