import React from 'react'
import classNamens from 'classnames'

const Categories = ({items, onClickItem}) => {

    const [activeItem, setActiveItem] = React.useState(null)

    const onSelectItem = index => {
        setActiveItem(index)
    }

    return(
        <div className="categories">
            <ul>
                <li onClick={() => onSelectItem(null)}
                    className={classNamens({'active': activeItem === null})}
                >Все</li>
                {items.map( (name, index) => {
                    return(
                        <li
                            onClick={() => onSelectItem(index)}
                            className={classNamens({'active': activeItem === index})}
                            key={`${name}_${index}`}
                        >{name}</li>
                    )
                })}

            </ul>
        </div>
    )
}

export default Categories