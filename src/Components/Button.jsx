import React from 'react'
import classNames from 'classnames'

const Button = ({outline, children}) => {
    return(
        <button className={classNames('Button.jsx', {
            'button--outline' : outline
        })}> {children}</button>
    )
}

export default Button