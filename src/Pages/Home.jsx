import React from 'react'
import {Categories, SortPopup, PizzaBlock} from "../Components";

const Home = ({items}) => {

    return(
        <div className="container">
            <div className="content__top">
                <Categories
                    onClickItem={ (name) => console.log(name) }
                    items={['Мясные','Вегетарианская','Гриль','Острые','Закрытые']} />

                <SortPopup items={[
                    {type:'popular', name:'популярности'},
                    {type: 'price', name:'цене'},
                    {type: 'alphabet', name:'алфавиту'}]} />

            </div>
            <h2 className="content__title">Все пиццы</h2>
            <div className="content__items">
                {
                    items.map( item =>  <PizzaBlock  {...item} key={item.id} /> )
                }


            </div>
        </div>
    )
}



export default Home