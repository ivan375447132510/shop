
const initialSatate = {
    items: [],
    isLoaded: false
}

const pizzasReducer = ( state = initialSatate, action ) => {
    switch (action.type) {
        case 'SET_PIZZAS':
            return{
                ...state,
                items: action.payload
            }
        default:
            return state
    }
}

export default pizzasReducer