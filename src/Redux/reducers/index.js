import {combineReducers} from 'redux'

import filterReducer from './filters'
import pizzasReducer from './pizzas'


const reducers = combineReducers({
    filter: filterReducer,
    pizzas: pizzasReducer
})

export default reducers